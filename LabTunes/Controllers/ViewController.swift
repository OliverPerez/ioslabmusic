//
//  ViewController.swift
//  LabTunes
//
//  Created by Oliver Jordy Pérez Escamilla on 11/9/18.
//  Copyright © 2018 Oliver Jordy Pérez Escamilla. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonWasTouchUpInside(_ sender: UIButton) {
        
        guard let userName = usernameTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        if User.login(yourusername: userName, password: password){
            
            performSegue(withIdentifier: "loginSucess", sender: self)
        }else {
            //Alert
        }
        
    }
    
}

