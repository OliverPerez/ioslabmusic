//
//  User.swift
//  LabTunes
//
//  Created by Oliver Jordy Pérez Escamilla on 11/9/18.
//  Copyright © 2018 Oliver Jordy Pérez Escamilla. All rights reserved.
//

import Foundation


class User {
    static let userName = "iOSLab"
    static let password = "verysecurepassword"
    static let session = Session.sharedInstance
    
    static func login(yourusername: String, password: String) -> Bool{
        if self.userName == yourusername {
            session.saveSession()
            return true
        }
       return false
    }
    
    static func fetchSongs() throws {
        guard let token = Session.sharedInstance.token else {
            throw UserError.notSessionAvailable
            
        }
        
    }
    enum UserError: Error {
        case notSessionAvailable
    }
}
