//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Oliver Jordy Pérez Escamilla on 11/9/18.
//  Copyright © 2018 Oliver Jordy Pérez Escamilla. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        let session = Session.sharedInstance
        session.token = nil
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCorrectLogin(){
        XCTAssertTrue(User.login(yourusername: "iOSLab", password: ""))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(yourusername: "Oliver", password: ""))
    }
    
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(yourusername: "iOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    
    func testSessionNil(){
        let session = Session.sharedInstance
        let _ = User.login(yourusername: "Oliver", password: "223")
        XCTAssertNil(session.token)
    }

    func testExpectedToken(){
        let _ = User.login(yourusername: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890", "Token Should Match")
    }
    
    func testThrowsError(){
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSongs(){
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fetchSongs { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
}
