//
//  MusicController.swift
//  LabTunes
//
//  Created by Oliver Jordy Pérez Escamilla on 11/10/18.
//  Copyright © 2018 Oliver Jordy Pérez Escamilla. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {
    
    var songs: [Song] = []
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
    }
    
    func downloadSongs(){
        Music.fetchSongs { (result: [Song]) in
            self.songs = result
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func downloadSongsBy(name: String){
        Music.fetchSongs(song: name) { (result: [Song]) in
            self.songs = result
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func setupSearchBar(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search songs"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
}

extension MusicViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "musicCell", for: indexPath)
       cell.textLabel?.text = songs[indexPath.row].name
        return cell
    }
}


extension MusicViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        downloadSongsBy(name: searchController.searchBar.text!)
    }
}
